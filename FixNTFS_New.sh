#!/bin/bash
#
#
echo "------------------"
echo "Unmounting Toshiba"
echo "------------------"
sudo umount /dev/sdg1    
sudo ntfsfix -b /dev/sdg1
sudo mount /dev/sdg1 /hdd/toshiba
sleep 1
#
echo "------------------"
echo "Unmounting WD Blue"
echo "------------------"
sudo umount /dev/sdh1   
sudo ntfsfix -b /dev/sdh1
sudo mount /dev/sdh1 /hdd/wd1tb
sleep 1
#
echo "-----------------"
echo "Unmounting Vertex"
echo "-----------------"
sudo umount /dev/sdb4    
sudo ntfsfix -b /dev/sdb4
sudo mount /dev/sdb4 /hdd/vertex
sleep 1
#
echo "------------------"
echo "Unmounting Element"
echo "------------------"
sudo umount /dev/sde1    
sudo ntfsfix -b /dev/sde1
sudo mount /dev/sde1 /hdd/element
#
echo "--------------------------"
echo "Drives should be fixed now"
echo "--------------------------"
#
#
#
